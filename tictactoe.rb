#!/home/hz/.rbenv/shims/ruby

class Board
  def initialize(dimensions = 3)
    puts "Generating board..."
    puts "Player 'X' goes first, followed by player 'O'."
    @player = 'x'
    @space = []
    @BOARD_DIMENSIONS = [dimensions, dimensions]
    @BOARD_DIMENSIONS[0].times do |x|
      @space[x] = {}
      @BOARD_DIMENSIONS[1].times do |y|
        @space[x][y] = 'empty'
      end
    end
    puts "Board size: #{@space.length}"
    puts " "
  end

  def displayBoard
    puts " "
    puts "        y1    y2    y3"
    puts "    ---------------------"
    puts "    |                   | "
    @space.length.times do |x|
      cols = []
      @space[x].length.times do |y|
        if @space[x][y] != 'empty'
          cols << "  #{@space[x][y]}  "
        else
          cols << "#{@space[x][y]}"
        end
      end
      layout = cols.join(" ")
      puts "x#{x + 1}  | #{layout} |"
      puts "    |                   | "
    end
    puts "    ---------------------"
    puts " "
  end

  def checkBoard
    # Check for row win
    @row_count = []
    @space.length.times do |x|
      @row_count[x] = []
      @space.length.times do |y|
          @row_count[x] << @space[x][y]
      end
      if @row_count[x].uniq.count == 1 && !@row_count[x].include?('empty')
        winning_player = @row_count[x][0[1]]
        return winning_player
      end
    end

    # Check for column win
    @column_count = []
    @space.length.times do |y|
      @column_count[y] = []
      @space.length.times do |x|
        @column_count[y] << @space[x][y[1]]
      end
      if @column_count[y].uniq.count == 1 && !@column_count[y].include?('empty')
        winning_player = @space[0][y[1]]
        return winning_player
      end
    end

    # Check for diagnoal win, top left to bottom right
    @diag_tl_to_br = []
    @space.length.times do |x|
      @diag_tl_to_br << @space[x][x]
    end
    if @diag_tl_to_br.uniq.count == 1 && !@diag_tl_to_br.include?('empty')
      winning_player = @space[0][0]
      return winning_player
    end
    # Check for diagnoal win, bottom left to top right
    @diag_bl_to_tr = []
    @space.length.times do |x|
      @diag_bl_to_tr << @space[(@space.length - x) - 1][x]
    end
    if @diag_bl_to_tr.uniq.count == 1 && !@diag_bl_to_tr.include?('empty')
      winning_player = @space[@space.length - 1][0]
      return winning_player
    end
  end

  def playRound
    self.displayBoard
    @player == 'o' ? @player = 'x' : @player = 'o'
    puts "#{@player}'s turn."
    getPlayerInput
    return checkBoard
  end

  def getPlayerInput
    x = getPlayerPosition('X') - 1
    y = getPlayerPosition('Y') - 1
    until !occupied?([x,y], @player)
      return getPlayerInput
    end
    return
  end

  def getPlayerPosition(axis)
    puts "Pick #{axis} coordinate position, 1 through #{@space.length}: "
    pos = gets.chomp.to_i
    if pos.is_a?(Integer) && pos <= @space.length && pos >= 1
      return pos
    else
      3.times { puts " " }
      puts "You picked #{pos}. That is an invalid #{axis} position. Try again."
      puts " "
      puts "#{@player}'s turn."
      self.displayBoard
      return getPlayerPosition(axis)
    end
  end

  def occupied?(coords, player)
    if @space[coords[0]][coords[1]] == 'empty'
      @space[coords[0]][coords[1]] = player
      return false
    else
      puts "Space already occupied. Pick another space."
      puts " "
      puts "#{@player}'s turn."
      return true
    end
  end

end

board = Board.new()
winner = nil
while !winner
 winner =  board.playRound
end
board.displayBoard
puts "#{winner.upcase} wins!!!"

